<?php
include "header.php";
?>

  <main id="main">

    <!-- ======= About Us Section ======= -->
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>About Us</h2>
          <ol>
            <li><a href="index.html">Home</a></li>
            <li>About Us</li>
          </ol>
        </div>

      </div>
    </section><!-- End About Us Section -->

    <!-- ======= About Section ======= -->
    <section class="about" data-aos="fade-up">
      <div class="container">

        <div class="row">
          <div class="col-lg-6">
            <img src="assets/img/about.jpg" class="img-fluid" alt="">
          </div>
          <div class="col-lg-6 pt-4 pt-lg-0">
            <h3>Perfect Frame</h3>
            <p class="font-italic">
            Some memories cannot be forgotten, especially the memories from your wedding remain vivid and touching throughout the life. So, we should treasure such memories in endearing pictures that would make us happy whenever we go through them. Perfect Frame knows the significance of memories in life and so document them in heart warming pictures and videos. The lively and adorable pictures and videos taken during your wedding, engagement or any important event will always keep the memories fresh in your mind.
The team of Perfect Frame comprises of creative and extremely professional photographers and cinematographers who can capture and transform moments into sweet memories through their imagination, experience and eye-catching pictures or videos. They do not miss any of the significant moment of the event and try to portray emotions through candid pictures. We just deliver perfect picture that would make a moment, event or ceremony extraordinary and superb.
We have made our comprehensive photography and cinematography services available in  Mumbai, Delhi, Goa, Jaipur, Bangalore, Surat, Vadodara .
            </p>
          </div>
        </div>

      <div class="row mt-3">
      <div class="col-lg-6 pt-4 pt-lg-0">
        <h5>The photography services that we provide summaries the following:</h5>
          <ul>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Wedding Photoshoot</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Pre-Wedding Photoshoot</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Pre-Wedding/Save the date Video</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Candid Wedding Photography</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Destination Wedding Photoshoot</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Traditional Wedding Photography</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Cinematic Wedding Video</li>
          <li class="font-italic"><i class="bx bx-chevron-right"></i>Traditional Wedding Video</li>
          </ul>
        </div>
        <div class="col-lg-6">
        <img src="assets/img/team/aashu.jpg" class="img-fluid" alt="" >
        </div>

      </div>
      
        
      </div>
    </section><!-- End About Section -->

    <!-- ======= Facts Section ======= -->
    <section class="facts section-bg" data-aos="fade-up">
      <div class="container">

        <div class="row counters">

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">232</span>
            <p>Clients</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">521</span>
            <p>Event Attended</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">1,463</span>
            <p>Hours Of Photography</p>
          </div>

          <div class="col-lg-3 col-6 text-center">
            <span data-toggle="counter-up">10</span>
            <p>Year's of Experience</p>
          </div>

        </div>

      </div>
    </section><!-- End Facts Section -->

    <!-- ======= Our Skills Section ======= -->
    <section class="skills" data-aos="fade-up">
      <div class="container">

        <div class="section-title">
          <h2>Our Skills</h2>
          <p>Photography is a way of feeling, of touching, of loving. What you have caught on film is captured forever... it remembers little things, long after you have forgotten everything.</p>
        </div>

        <div class="skills-content">

          <div class="progress">
            <div class="progress-bar bg-success" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Creativity <i class="val">100%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-info" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Technical photography skills <i class="val">90%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-warning" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Attention to detail <i class="val">75%</i></span>
            </div>
          </div>

          <div class="progress">
            <div class="progress-bar bg-danger" role="progressbar" aria-valuenow="55" aria-valuemin="0" aria-valuemax="100">
              <span class="skill">Team working skills <i class="val">55%</i></span>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Our Skills Section -->

  </main><!-- End #main -->

 <?php
include "footer.php";
?>
