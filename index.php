<?php
session_start();
error_reporting(0);

include './connection.php';

$name = $_SESSION['Useremail'];
if($name != ''){
  $res = mysqli_query($conn, "select * from bookStatus where user_name='$name'");
  if(mysqli_num_rows($res) >= 1){
      while($row = mysqli_fetch_assoc($res)){
        $status = $row['status'];
      }
    }
    if($status == "Accept"){
        $st = '<div class="alert alert-success container .alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Accepted!</strong> Your Booking request is accepted by company for more info contact us.
      </div>';
    }
    else if($status == "Reject"){
      $st = '<div class="alert alert-success container .alert-dismissible">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Rejected!</strong> Your Request is rejected for more info contact us.
    </div>';
    }
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <title>Perfect Frame</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

   <!-- Jquery CDN -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <!-- Sweat Alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>
<?php
                $VideoDirectory = "./assets/Video/";
                $videoPath = [];
                if(is_dir($VideoDirectory))
                {
                  $opendirectory = opendir($VideoDirectory);
                    $i = 0;
                    while (($Video = readdir($opendirectory)) !== false)
                  {
                    if(($Video == '.') || ($Video == '..'))
                    {
                      continue;
                    }
                    $videoPath[$i] = $Video;
                    $i++;
                 
                  }
                }
                    closedir($opendirectory);
                
?>

<script>
function login_notice()
{
  swal("Before Booking Please Sign in OR Register.");
}

var videoPath = <?php echo json_encode($videoPath); ?>;
var VideoCount = 0;

function previousVideo(){
  if(VideoCount >= 0 && VideoCount <= videoPath.length-1){
  document.getElementById("videochange").src = "./assets/Video/".concat(videoPath[VideoCount]);
  VideoCount--;
  }
  else{
    VideoCount = videoPath.length-1;
  }
}
function nextVideo(){
  if(VideoCount >= 0 && VideoCount <= videoPath.length-1){
    document.getElementById("videochange").src = "./assets/Video/".concat(videoPath[VideoCount]);
    VideoCount++;
  }
  else{
    VideoCount = 0;
  }
}
</script>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top header-transparent">
    <div class="container">

      <div class="logo float-left">
        <h1 class="text-light"><a href="index.php"><span>Perfect Frame</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.php"><img src="./logo.png" alt="" class="img-fluid" width="200" height="200"></a> -->
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <li class="active"><a href="index.php">Home <i class="la la-angle-down"></i></a></li>
          <li><a href="about.php">About Us</a></li>
          <li class="drop-down"><a href="#">Services</a>
          <ul>
                  <li><a href="services.php" onclick="Services(this)">Wedding</a></li>
                  <li><a href="services.php" onclick="Services(this)">Pre-Wedding</a></li>
                  <li><a href="services.php" onclick="Services(this)">Maternity</a></li>
                  <li><a href="services.php" onclick="Services(this)">Portrait</a></li>
                  <li><a href="services.php" onclick="Services(this)">Kids</a></li>
                  <li><a href="services.php" onclick="Services(this)">Corporate</a></li>
                  <li><a href="services.php" onclick="Services(this)">Events</a></li>
            </ul>
          </li>
          <li><a href="team.php">Our Team</a></li>
          <li><a href="contact.php">Contact Us</a></li>
          <?php if(isset($_SESSION['name'])) { ?>
          <li><a id="logout" href="logout.php"><?php echo strtoupper($_SESSION['name']); ?></a></li>
          <?php }else{ ?>
         
          <li style="cursor: pointer;"><a data-toggle="modal" data-target="#LoginModel">Login</a></li>
          <li style="cursor: pointer;"><a data-toggle="modal" data-target="#RegisterModel">Register</a></li>
          <?php } ?>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->

  <div id="carouselExampleControls" class="carousel slide" data-ride="carousel" style="clip-path: polygon(0% 0%, 100% 0%, 100% 100%, 0% 90%);
">
  <div class="carousel-inner" >
    <div class="carousel-item active">
      <img class="d-block w-100" src="./assets/img/slide/second.jpg" alt="First slide">
      <div class="carousel-caption ">
      <h2 class="animated fadeInDown font-italic">Welcome to <span>Perfect Frame</span></h2>
          <p class="animated fadeInUp font-italic">Only photography has been able to divide human life into a series of moments, each of them has the value of a complete existence.</p>
      <?php if(isset($_SESSION['name'])) { ?>
  <button class="btn btn-outline-primary" data-toggle="modal" data-target="#booking">Booking</button>
  <?php }else{ ?>
  <button class="btn btn-outline-primary" data-toggle="modal" onclick='login_notice()'>Booking</button>
  <?php } ?>
  </div>
  </div>

    <div class="carousel-item">
      <img class="d-block w-100" src="./assets/img/slide/third.jpg" alt="Second slide">
      <div class="carousel-caption ">
      <h2 class="animated fadeInDown font-italic text-dark">What Perfect Frame offers</h2>
      <p class="animated fadeInUp font-italic text-dark">We offers different types of videography and photography. in which some popular photography is wedding, coroporate, industrial, exterior etc.</p>
      <?php if(isset($_SESSION['name'])) { ?>
  <button class="btn btn-outline-primary" data-toggle="modal" data-target="#booking">Booking</button>
  <?php }else{ ?>
  <button class="btn btn-outline-primary" data-toggle="modal" onclick='login_notice()'>Booking</button>
  <?php } ?>
  </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100" src="./assets/img/slide/second.jpg" alt="Third slide">
      <div class="carousel-caption ">
      <h2 class="animated fadeInDown font-italic">Why Choose us</h2>
        <p class="animated fadeInUp font-italic">What do we feel when we look at a good photograph? We just want to be there, right at the exact moment that photo taken.</p>
      <?php if(isset($_SESSION['name'])) { ?>
  <button class="btn btn-outline-primary" data-toggle="modal" data-target="#booking">Booking</button>
  <?php }else{ ?>
  <button class="btn btn-outline-primary" data-toggle="modal" onclick='login_notice()'>Booking</button>
  <?php } ?>
  </div>

    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>


  <main id="main">
            
    <!-- ======= Services Section ======= -->
    <section class="services">
      <div class="container">
     <?php echo $st; ?>
        <div class="row">
          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up">
            <div class="icon-box icon-box-pink">
              <div class="icon"><i class="bx bx-heart"></i></div>
              <h4 class="title"><a href="">Wedding Photography</a></h4>
              <p class="description">Photography of commercial and residential, architects, real estate companies and construction professionals.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
            <div class="icon-box icon-box-cyan">
              <div class="icon"><i class="bx bx-parents"></i></div>
              <h4 class="title"><a href="">Kids Photography</a></h4>
              <p class="description">Photography of commercial and residential, architects, real estate companies and construction professionals.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-green">
              <div class="icon"><i class="bx bx-tachometer"></i></div>
              <h4 class="title"><a href="">Corporate Photography</a></h4>
              <p class="description">photography of the corporate workspace to provide images for business websites and for all promotional purposes.</p>
            </div>
          </div>

          <div class="col-md-6 col-lg-3 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
            <div class="icon-box icon-box-blue">
              <div class="icon"><i class="bx bx-world"></i></div>
              <h4 class="title"><a href="">Events Photography</a></h4>
              <p class="description">Photography of  industrial plants and manufacturing processes, refineries, civil / infrastructure projects, institutional and recreational projects</p>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End Services Section -->

    <!-- ======= Why Us Section ======= -->
    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
      <div class="container">

      <div class="embed-responsive embed-responsive-21by9">
      <iframe class="embed-responsive-item" src="./assets/Video/VID_20200320004053.mp4" id="videochange"></iframe>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true" onclick="previousVideo()"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true" onclick="nextVideo()"></span>
    <span class="sr-only">Next</span>
  </a>
      </div>
      </div>
    </section><!-- End Why Us Section -->

    <section class="testimonials" data-aos="fade-up">
      <div class="container">

        <div class="owl-carousel testimonials-carousel">        
          <div class="testimonial-item">
            <img src="assets/img/owner.jpeg" style="border-radius:50%; border :4px solid #fff; margin:0 auto; width: 250px; height:250px" alt="">
            <h3>Aashu Kanodia</h3>
            <h4>Ceo &amp; Founder</h4>
            <p>
              <i class="bx bxs-quote-alt-left quote-icon-left"></i>
              I am a freelance photographer based in Mumbai. I love shooting couples in the quirkiest possible manner with my ‘self-proclaimed’ sense of humour. My love of imagery began as an extension of my love for storytelling.
Having achieved that goal, I realized that telling a good story meant “painting” pictures in the minds of the readers. Creating actual images was the natural progression. I’ve been cultivating my love of imagery ever since.
When I'm not chasing light, I love spending time with my family, reading, making music and travelling.
              <i class="bx bxs-quote-alt-right quote-icon-right"></i>
            </p>
          </div>
      </div>
    </div>
    </section>


    <section class="why-us section-bg" data-aos="fade-up" date-aos-delay="200">
      <div class="container">
      <p class="font-italic text-center mb-2" style="font-size:20px;">Candid And Traditional Photographer In Mumbai</p>
      <div class="row">
      <div class="col-lg-6 story">
            <img src="./assets/img/services/Wedding/-20200319-0005.jpg" alt="">
      </div>

      <div class="col-lg-6 story">
              <img src="./assets/img/services/Event/zillionevents-20200319-0015.jpg" alt="">
      </div>
      </div>
      </div>
  </section>

  </main><!-- End #main -->
<script>
  function Services(link){
  localStorage.setItem("BtnName",link.innerHTML);
}

</script>

<style>
  .story img{
    height:400px;
    width:100%;
    max-height:400px;
    max-width:100%;
  }
</style>
  <!-- ======= Footer ======= -->
  <?php
include "UserModal.php";
include "footer.php";
?>
