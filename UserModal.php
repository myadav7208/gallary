  <!--Login  Modal -->
<div  class="modal fade" id="LoginModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div  class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Login</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form>
        <div class="form-group">
            <input type="email" class="form-control" id="loginmail" aria-describedby="emailHelp" placeholder="Enter Your Email*">
      </div>
      <div class="form-group">
          <input type="password" class="form-control" id="loginpass" placeholder="Enter Your Password*">
      </div>
    </form>
      </div>
      <div class="modal-footer">
        <span id="errorlogin"></span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="login" type="button" class="btn btn-info">Login</button>
      </div>
    </div>
  </div>
</div>





  <!--Register Modal -->
<div class="modal fade" id="RegisterModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Register</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form>
           <div class="form-group">
            <input type="text" class="form-control" id="fname" aria-describedby="emailHelp" placeholder="Full Name*">
            <span id="errorfname"></span>
      </div>
        <div class="form-group">
            <input type="email" class="form-control" id="mail" aria-describedby="emailHelp" placeholder="Email*">
            <span id="erroremail"></span>
      </div>
       <div class="form-group">
            <input type="text" class="form-control" id="mob" aria-describedby="emailHelp" placeholder="Mobile No*">
            <span id="errormob"></span>
      </div>
      <div class="form-group">
          <input type="password" class="form-control" id="pass" placeholder="Password*">
          <span id="errorpass"></span>
      </div>
      <div class="form-group">
          <input type="password" class="form-control" id="cpass" placeholder="Re-enter Password*">
          <span id="errorcpass"></span>
      </div>
    </form>
      </div>
      <div class="modal-footer">
        <span id="errorsubmit"></span>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        <button id="register" type="button" class="btn btn-info">Register</button>
      </div>
    </div>
  </div>
</div>


<!-- Booking modal -->
<div class="modal fade right" id="booking" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-full-height modal-right modal-notify modal-info" role="document">
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <p class="heading lead">Booking Request <span id="bookingError"></span>
        </p>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true" class="white-text">×</span>
        </button>
      </div>

      <!--Body-->
      <div class="modal-body">
      <div class="form-group">
      <label for="event-date">Event Date </label>
          <input type="date" class="form-control" id="event_date">
      </div>

      <div class="form-group">
      <label for="event-date">Event Place </label>
          <input type="text" class="form-control" id="event_place" placeholder="Place*">
      </div>
        <!--Basic textarea-->
        <div class="md-form">
          <textarea type="text" id="bookingmsg" class="md-textarea form-control" rows="3"></textarea>
          <label for="form79textarea">Your message</label>
        </div>
        <span id="bookingCon"></span>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <a type="button" class="btn btn-primary waves-effect waves-light" id="bookingConfirm">Send
          <i class="fa fa-paper-plane ml-1"></i>
        </a>
        <a type="button" class="btn btn-outline-primary waves-effect" data-dismiss="modal">Cancel</a>
      </div>
    </div>
  </div>
</div>
<!-- Modal: modalPoll -->


<script src="./assets/js/register.js"></script>
<script src="./assets/js/login.js"></script>
<script src="./assets/js/validation.js"></script>
<script src="./assets/js/booking.js"></script>
