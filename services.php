<?php
include "header.php";
?>

<style>
.allimag img{
  height : 300%;
  width : 350px;
  max-height : 280px;
  max-width : 350px;
}
</style>

  <main id="main">

   
    <section class="breadcrumbs">
      <div class="container">

        <div class="d-flex justify-content-between align-items-center">
          <h2>Our Services</h2>
          <ol>
            <li><a href="index.html">Home</a></li>
            <li>Our Services</li>
          </ol>
        </div>

      </div>
    </section>

    <!-- ======= Gallary Section ======= -->
    <section class="portfolio">
      <div class="container">

        <div class="row">
          <div class="col-lg-12">
            <ul id="portfolio-flters">
              <!-- <li data-filter="*" class="filter-active">All</li> -->
              <li data-filter=".filter-Wedding" id="Wedding">Wedding</li>
              <li data-filter=".filter-PreWedding" id="Pre-Wedding">Pre-Wedding</li>
              <li data-filter=".filter-Maternity" id="Maternity">Maternity</li>
              <li data-filter=".filter-Portrait" id="Portrait">Portrait</li>
              <li data-filter=".filter-Kids" id="Kids">Kids</li>
              <li data-filter=".filter-Corporate" id="Corporate">Corporate</li>
              <li data-filter=".filter-Events" id="Events">Events</li>
            </ul>
          </div>
        </div>

        <div class="row portfolio-container" data-aos="fade-up" data-aos-easing="ease-in-out" data-aos-duration="500">

          
            <?php 
                foreach(glob("./assets/img/services/Portrait/{*.jpg,*.png,*.jpeg}", GLOB_BRACE) as $image1){
                echo '<div class="col-lg-4 col-md-6 filter-Portrait allimag">
                      <div class="portfolio-item">
                      <img src="'.$image1.'" class="img-fluid" alt="">
                      <div class="portfolio-info">
                      <h3><a href="'.$image1.'" data-gall="portfolioGallery" class="venobox" title="Portrait">Portrait</a></h3>
                      <a href="'.$image1.'" data-gall="portfolioGallery" class="venobox" title="Portrait"><i class="icofont-plus"></i></a>
                    </div>
                  </div>
                </div>';
                }
                ?>

                <?php
                $imagesDirectory = "./assets/img/services/corporate/";
                if(is_dir($imagesDirectory))
                {
                  $opendirectory = opendir($imagesDirectory);
                  
                    while (($image = readdir($opendirectory)) !== false)
                  {
                    if(($image == '.') || ($image == '..'))
                    {
                      continue;
                    }
                    $imgFileType = pathinfo($image,PATHINFO_EXTENSION);
                    if(($imgFileType == 'JPG') || ($imgFileType == 'JPEG'))
                    {
                      echo '<div class="col-lg-4 col-md-6 filter-Corporate">
                      <div class="portfolio-item">
                        <img src="assets/img/services/corporate/'.$image.'" class="img-fluid" alt="">
                        <div class="portfolio-info">
                          <h3><a href="assets/img/services/corporate/'.$image.'" data-gall="portfolioGallery" class="venobox" title="Corporate">Corporate</a></h3>
                          <a href="assets/img/services/corporate/'.$image.'" data-gall="portfolioGallery" class="venobox" title="Corporate"><i class="icofont-plus"></i></a>
                        </div>
                      </div>
                    </div>';
                    }
                    }
                    closedir($opendirectory);
                }
                ?>

                <?php 
                foreach(glob("./assets/img/services/Event/{*.jpg,*.png,*.jpeg}", GLOB_BRACE) as $image3){
                echo '<div class="col-lg-4 col-md-6 filter-Events allimag">
                      <div class="portfolio-item">
                      <img src="'.$image3.'" class="img-fluid" alt="">
                      <div class="portfolio-info">
                      <h3><a href="'.$image3.'" data-gall="portfolioGallery" class="venobox" title="Events">Events</a></h3>
                      <a href="'.$image3.'" data-gall="portfolioGallery" class="venobox" title="Events"><i class="icofont-plus"></i></a>
                    </div>
                  </div>
                </div>';
                }
                ?>

              <?php 
                foreach(glob("./assets/img/services/Kids/{*.jpg,*.png,*.jpeg}", GLOB_BRACE) as $image4){
                echo '<div class="col-lg-4 col-md-6 filter-Kids allimag">
                      <div class="portfolio-item">
                      <img src="'.$image4.'" class="img-fluid" alt="">
                      <div class="portfolio-info">
                      <h3><a href="'.$image4.'" data-gall="portfolioGallery" class="venobox" title="Kids">Kids</a></h3>
                      <a href="'.$image4.'" data-gall="portfolioGallery" class="venobox" title="Kids"><i class="icofont-plus"></i></a>
                    </div>
                  </div>
                </div>';
                }
                ?>

              <?php 
                foreach(glob("./assets/img/services/Prewedding/{*.jpg,*.png,*.jpeg}", GLOB_BRACE) as $image5){
                echo '<div class="col-lg-4 col-md-6 filter-PreWedding allimag">
                      <div class="portfolio-item">
                      <img src="'.$image5.'" class="img-fluid" alt="">
                      <div class="portfolio-info">
                      <h3><a href="'.$image5.'" data-gall="portfolioGallery" class="venobox" title="Pre-Wedding">Pre-Wedding</a></h3>
                      <a href="'.$image5.'" data-gall="portfolioGallery" class="venobox" title="Pre-Wedding"><i class="icofont-plus"></i></a>
                    </div>
                  </div>
                </div>';
                }
                ?>

              <?php 
                foreach(glob("./assets/img/services/Wedding/{*.jpg,*.png,*.jpeg}", GLOB_BRACE) as $image6){
                echo '<div class="col-lg-4 col-md-6 filter-Wedding allimag">
                      <div class="portfolio-item">
                      <img src="'.$image6.'" class="img-fluid" alt="">
                      <div class="portfolio-info">
                      <h3><a href="'.$image6.'" data-gall="portfolioGallery" class="venobox" title="Wedding">Wedding</a></h3>
                      <a href="'.$image6.'" data-gall="portfolioGallery" class="venobox" title="Wedding"><i class="icofont-plus"></i></a>
                    </div>
                  </div>
                </div>';
                }
                ?>

              <?php 
                foreach(glob("./assets/img/services/Maternity/{*.jpg,*.png,*.jpeg}", GLOB_BRACE) as $image7){
                echo '<div class="col-lg-4 col-md-6 filter-Maternity allimag">
                      <div class="portfolio-item">
                      <img src="'.$image7.'" class="img-fluid" alt="">
                      <div class="portfolio-info">
                      <h3><a href="'.$image7.'" data-gall="portfolioGallery" class="venobox" title="Maternity">Maternity</a></h3>
                      <a href='.$image7.'" data-gall="portfolioGallery" class="venobox" title="Maternity"><i class="icofont-plus"></i></a>
                    </div>
                  </div>
                </div>';
                }
                
              ?>
 
        </div>

      </div>
    </section><!-- End Portfolio Section -->

  </main><!-- End #main -->
  <script>
    var select = "#".concat(localStorage.getItem("BtnName"));
    
    
    $(document).ready(function() {

        $(".filterbtn").click(function(){
            let Id = this.id;
            let s = Id[0].toUpperCase() +  Id.slice(1); 
            $("#".concat(s)).click();
        });

        setTimeout(function(){
          $(select).click();
    }, 2000);
    localStorage.removeItem('BtnName');
    });
</script>
  <!-- ======= .phpter ======= -->
<?php
include "footer.php";
?>
