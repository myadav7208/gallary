$(document).ready(function(){
    $("#login").click(function(){

        let email1 = $("#loginmail").val();
        let pass1 = $("#loginpass").val();
        console.log(email1, pass1)
        if(email1 == "" || pass1 == ""){
            $("#errorlogin").text("Please Fill all field");
            $("#errorlogin").css("color", "red")
        }
        else{
            $.ajax({
                method: "POST",
                url: "operation.php",
                data: ({ email1:email1, pass1:pass1 }),
                success: function(data) {
                if(data == "failed"){
                    $("#errorlogin").text("Incorrect Details");
                    $("#errorlogin").css("color", "red");  
              }
              else{
                $("#errorlogin").text("login Successfully.");
                $("#errorlogin").css("color", "green");
                setTimeout(function(){ window.location = "index.php"; }, 1000); 
              
              }

        }

      });
        }
    });

    var logoutStatus;
    $("#logout").mouseenter(function(){
        logoutStatus = $("#logout").text();
        $("#logout").text("Logout");
    });
    $("#logout").mouseleave(function(){
        $("#logout").text(logoutStatus);
    });
});