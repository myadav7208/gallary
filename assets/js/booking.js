$(document).ready(function(){
    $("#bookingConfirm").click(function(){
       
        let bdate = $("#event_date").val();
        let bplace = $("#event_place").val();
        let bmsg = $("#bookingmsg").val();
        
        if(bdate == "" || bplace == ""){
            $("#bookingError").text("Please Fill all field");
            $("#bookingError").css("color", "red")
        }
        else{
            $.ajax({
                method: "POST",
                url: "operation.php",
                data: ({ edate:bdate, eplace:bplace, emsg:bmsg }),
                success: function(data) {
                if(data == "failed"){
                    $("#bookingCon").text("Something Went Wrong.");
                    $("#bookingCon").css("color", "red");  
              }
              else{
                $("#event_date").val("");
                $("#event_place").val("");
                $("#event_place").val("");
                $("#bookingCon").text("Requested Successfully.");
                $("#bookingCon").css("color", "green");
              }

        }

      });
        }
    });

});