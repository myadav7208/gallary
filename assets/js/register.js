$(document).ready(function(){
    $("#register").click(function(){
        let fname = $('#fname').val();
        let mob = $("#mob").val();
        let email = $("#mail").val();
        let pass = $("#pass").val();
        let cpass = $("#cpass").val();
       
        if(fname == "" || mob == "" || email == "" || pass == "" || cpass == ""){
            $("#errorsubmit").text("Please Fill all required field");
            $("#errorsubmit").css("color", "red")
        }
        else{
            $.ajax({
                method: "POST",
                url: "operation.php",
                data: ({ fname: fname, mob:mob, email:email, pass:pass, cpass:cpass }),
                success: function(data) { 
                if(data != "success"){
                    $("#errorsubmit").text(data);
                    $("#errorsubmit").css("color", "red"); 
              }
              else{
                $("#errorsubmit").text("Registered Successfully.");
                $("#errorsubmit").css("color", "green"); 
                $("input[type=text]").val("");
                $("input[type=email]").val("");
                $("input[type=password]").val(""); 
              }

        }

      });
        }
    });
});