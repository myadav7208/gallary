$(document).ready(function(){
    $('#fname').keyup(function(e){
       e.preventDefault();
  
     // console.log(".fname");
      let fname = $('#fname').val();
      var namere = /^[A-Za-z]+$/;
  
      // first name validate start 
      if(fname == ""){
       $("#errorfname").text("First Name cannot be empty");
       $("#errorfname").css("color", "red");
       return false;
      }
      else{
        $("#errorfname").text("");
        $("#errorfname").css("color", "");
      }
      if(!namere.test(fname)){
        $("#errorfname").text("First Name should be alphabate only");
        $("#errorfname").css("color", "red");
        return false;
      }
      else{
         $("#errorfname").text("");
     
      }
      // first name validate end
    });
  
  // last name start
  
  $('#lname').keyup(function(e){
       e.preventDefault();
  
     // console.log(".fname");
      let lname = $('#lname').val();
      var namere = /^[A-Za-z]+$/;
  
      if(lname == ""){
          $("#errorlname").text("Last Name");
         $("#errorlname").css("color", "");
       return false;
      }
  
      if(!namere.test(lname)){
        $("#errorlname").text("Last Name should be alphabate only");
        $("#errorlname").css("color", "red");
        return false;
      }
      else{
         $("#errorlname").text("");
       
      }
     
    });
  
  // last name end
     
      //mobile number validate start
      $("#mob").keyup(function(e){
        e.preventDefault();
        let mob = $("#mob").val();
        var mobre = /^[0-9]+$/;
        var phoneno = /^\d{10}$/;
      
        if(!mobre.test(mob)){
          $("#errormob").text("Mobile Should be digit only");
          $("#errormob").css("color", "red");
          return false;
        }
        else{
          $("#errormob").text("Mobile No*");
          $("#errorlname").css("color", "");
        }
        if(mob.toString().length <= 9 || mob.toString().length > 10){
            $("#errormob").text("Invalid Mobile number");
            $("#errormob").css("color", "red");
            return false;
        }
        
        else{
          $("#errormob").text("");
     
        }
        
      });
      //mobile number validate end
  
    
  
  
       //email validate start
      $("#mail").keyup(function(e){
        e.preventDefault();
        let email = $("#mail").val();
        var emailreg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
       
  
       if(email.toString().length == 0){
           $("#erroremail").text("Email Address*");
           $("#erroremail").css("color", "");
           return false;
          
        }
        if(!emailreg.test(email)){
          $("#erroremail").text("Invalid Email id");
          $("#erroremail").css("color", "red");
          return false;
        }
        else{
          $("#erroremail").text("");
      
        }
  
        
      });
       //email validate end
  
       //password validate start
      $("#pass").keyup(function(e){
        e.preventDefault();
      
        let pass = $("#pass").val();
  
         if(pass.toString().length == 0){
           $("#errorpass").text("Please Enter Password");
           $("#errorpass").css("color", "red");
           return false; 
        }
        else{
           $("#errorpass").text("Password*");
           $("#errorpass").css("color", "");
        }
  
       if(pass.toString().length <= 5){
           $("#errorpass").text("Password length should be minimum 6.");
           $("#errorpass").css("color", "red");
           return false; 
        }
        else{
           $("#errorpass").text("");
        
        }
        
      });
       //password validate end
  
       $("#cpass").keyup(function(e){
        e.preventDefault();
        let cpass = $("#cpass").val();
        let pass = $("#pass").val();
  
         if(cpass != pass){
           $("#errorcpass").text("Confirm Password Not Matching");
           $("#errorcpass").css("color", "red");
           return false; 
        }
        else{
           $("#errorcpass").text("");
         
        }
      });
  });