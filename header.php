<?php
session_start();
error_reporting(0);
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Perfect Frame</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Jquery CDN -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container">

      <div class="logo float-left">
        <h1 class="text-light"><a href="index.php"><span>Perfect Frame</span></a></h1>
    
        <!-- <a href="index.php"><img src="logo.png" alt="" class="img-fluid"></a> -->
      </div>

      <nav class="nav-menu float-right d-none d-lg-block">
        <ul>
          <li><a href="index.php">Home <i class="la la-angle-down"></i></a></li>
          <li><a href="about.php">About Us</a></li>
          <li class="drop-down"><a href="#">Services</a>
          <ul>
                  <li><a href="services.php" onclick="Services(this)" id="wedding" class="filterbtn">Wedding</a></li>
                  <li><a href="services.php" onclick="Services(this)" id="pre-Wedding" class="filterbtn">Pre-Wedding</a></li>
                  <li><a href="services.php" onclick="Services(this)" id="maternity" class="filterbtn">Maternity</a></li>
                  <li><a href="services.php" onclick="Services(this)" id="portrait" class="filterbtn">Portrait</a></li>
                  <li><a href="services.php" onclick="Services(this)" id="kids" class="filterbtn">Kids</a></li>
                  <li><a href="services.php" onclick="Services(this)" id="corporate" class="filterbtn">Corporate</a></li>
                  <li><a href="services.php" onclick="Services(this)" id="events" class="filterbtn">Events</a></li>
            </ul>
          </li>
          <li><a href="team.php">Our Team</a></li>
          <li><a href="contact.php">Contact Us</a></li>
          <?php if(isset($_SESSION['name'])) { ?>
               <li><a id="logout" href="logout.php"><?php echo strtoupper($_SESSION['name']); ?></a></li>
          <?php }else{ ?>
          <li style="cursor: pointer;"><a data-toggle="modal" data-target="#LoginModel">Login</a></li>
          <li style="cursor: pointer;"><a data-toggle="modal" data-target="#RegisterModel">Register</a></li>
          <?php } ?>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->

<script>
function Services(link){
  localStorage.setItem("BtnName",link.innerHTML);
}

</script>
<?php
include "UserModal.php";
?>