-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2020 at 07:32 AM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gallery`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`name`, `email`, `password`) VALUES
('admin', 'admin@gmail.com', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `name` varchar(20) NOT NULL,
  `email` varchar(25) NOT NULL,
  `edate` date NOT NULL,
  `eplace` varchar(50) NOT NULL,
  `emsg` varchar(300) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`name`, `email`, `edate`, `eplace`, `emsg`, `id`) VALUES
('MANISH', 'manishyadav7208@gmail.com', '2021-02-02', 'mumbai', 'shadi', 5),
('akash', 'akash@gmail.com', '2021-04-19', 'tyhbfds', 'event', 7),
('manish', 'manish@gmail.com', '2021-02-02', 'sh', 'sdn', 8);

-- --------------------------------------------------------

--
-- Table structure for table `bookstatus`
--

CREATE TABLE `bookstatus` (
  `id` int(11) NOT NULL,
  `user_name` varchar(30) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bookstatus`
--

INSERT INTO `bookstatus` (`id`, `user_name`, `status`) VALUES
(19, 'manishyadav7208@gmail.com', 'Accept'),
(20, 'akash@gmail.com', 'Reject'),
(21, 'manish@gmail.com', 'Accept');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `Id` int(11) NOT NULL,
  `Full_Name` varchar(20) NOT NULL,
  `Email` varchar(30) NOT NULL,
  `sub` varchar(150) DEFAULT NULL,
  `Msg` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`Id`, `Full_Name`, `Email`, `sub`, `Msg`) VALUES
(1, 'MANISH', 'manishyadav7208@gmail.com', NULL, 'tsting'),
(2, 'MANISH', 'manishyadav7208@gmail.com', NULL, 'i have some doubt.'),
(3, 'MANISH', 'manishyadav7208@gmail.com', NULL, 'i am testing this. site'),
(4, 'MANISH', 'manishyadav7208@gmail.com', NULL, 'sdfgh'),
(5, 'Manish', 'sp313@gmail.com', 'qwer', 'asdfrgtbh');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `Id` int(11) NOT NULL,
  `Full_Name` varchar(25) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Mobile` bigint(20) NOT NULL,
  `Password` varchar(25) NOT NULL,
  `Confirm_Pass` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`Id`, `Full_Name`, `Email`, `Mobile`, `Password`, `Confirm_Pass`) VALUES
(11, 'akash', 'akash@gmail.com', 321654987, '123456', '123456'),
(9, 'ankit', 'ankit@gmail.com', 9874563215, '123456', '123456'),
(7, 'ANKIT', 'm.yadav7208@gmail.com', 8424982846, '1234567', '1234567'),
(2, 'manish', 'manish@gmail.com', 9874563210, '123456', '123456'),
(6, 'MANISH', 'manishyadav720@gmail.com', 8424982845, '123456', '123456'),
(13, 'shyam', 'shyam@gmail.com', 1111222222, '123456', '123456');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bookstatus`
--
ALTER TABLE `bookstatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`Email`),
  ADD UNIQUE KEY `Id` (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bookstatus`
--
ALTER TABLE `bookstatus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
