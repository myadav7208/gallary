<?php
  include '../connection.php';
  $rowdata ='';
  $res = mysqli_query($conn, "select * from booking where email not in (select user_name from bookStatus)");
    if(mysqli_num_rows($res) >= 1){
        while($row = mysqli_fetch_assoc($res)){
          $rowdata .="<tr>
          <td>".$row['name']."</td>
          <td>".$row['edate']."</td>
          <td>".$row['eplace']."</td>
          <td>".$row['emsg']."</td>
          <td>
          <button type='button' class='btn btn-success' value=".$row['email'].">Accept</button>
          <button type='button' class='btn btn-warning' value=".$row['email'].">Reject</button>
          </td>
          </tr>"; 
      
        }
    }
    else{
        echo "No Booking Details Found";
    }
    session_start();

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Perfect Frame</title>
  <meta content="" name="descriptison">
  <meta content="" name="keywords">

  <!-- Jquery CDN -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,700,700i&display=swap" rel="stylesheet">
    <link href="">
  <!-- Vendor CSS Files -->
  <link href="../assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="../assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="../assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="../assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="../assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="../assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="../assets/vendor/aos/aos.css" rel="stylesheet">
  <!-- Sweat Alert -->
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  

  <!-- Template Main CSS File -->
  <link href="../assets/css/style.css" rel="stylesheet">

</head>

<body>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: center;
  padding: 8px;
}


</style>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container">

      <div class="logo float-left">
        <h1 class="text-light"><a href="index.php"><span>Perfect Frame</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.php"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <div class="float-right">
        <h4 style="color:white; display:block;" id="admin_name"><?php echo $_SESSION['name']; ?></h4>
        <form method='post' action="" style="display:none;" id="logout_btn">
            <input type="submit" value="Logout" name="but_logout">
        </form>
     
      </div>


    </div>
  </header><!-- End Header -->
<div class="container" style="margin-top:100px;">
<table>
  <tr>
    <th>Full Name</th>
    <th>Event Date</th>
    <th>Event Place</th>
    <th>Message</th>
    <th>Action</th>
  </tr>
  <?php echo $rowdata; ?>
</table>

</div>


 

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="../assets/vendor/jquery/jquery.min.js"></script>
  <script src="../assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="../assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="../assets/vendor/php-email-form/validate.js"></script>
  <script src="../assets/vendor/venobox/venobox.min.js"></script>
  <script src="../assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="../assets/vendor/counterup/counterup.min.js"></script>
  <script src="../assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="../assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="../assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="../assets/js/main.js"></script>
  <script src="./js/booking_detail.js"></script>
</body>
<?php
if(isset($_POST['but_logout'])){
  session_destroy();
  header('Location: index.php');
}
?>

<script>
$(document).ready(function () {
    $("#admin_name").mouseenter(function () {

        $("#admin_name").css("display", "none");
        $("#logout_btn").css("display", "block");
    });

    $("#logout_btn").mouseleave(function () {

      $("#admin_name").css("display", "block");
      $("#logout_btn").css("display", "none");
  });
});


$(".btn").click(function() {
  let btnVal = $(this).prop("value");
  let btnName = $(this).prop("innerHTML");



  $(this).closest('tr').remove();

  $.ajax({
                method: "POST",
                url: "./adminopt.php",
                data: ({ userid:btnVal, status:btnName }),
                success: function(data) {
                 if(btnName == "Accept"){
                  swal("Good job!", "You Accepted!", "success");
              }
              else{
                swal("Ohh!", "You Rejected!", "error");
              }
        }

      });
  
  
    });
</script>
</html>